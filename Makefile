CC=gcc
CFLAGS=-I.


va_pid_to_pa: va_pid_to_pa.c reusable.c
	     $(CC) -o va_pid_to_pa.out va_pid_to_pa.c reusable.c  -O3

is_my_pa: is_my_pa.c reusable.c
	     $(CC) -o is_my_pa.out is_my_pa.c reusable.c  -O3 -fopenmp

dummy_proc: dummy_proc.c 
	    $(CC) -o dummy_proc.out dummy_proc.c

all: va_pid_to_pa dummy_proc is_my_pa

.PHONY: clean all

.DEFAULT_GOAL := all

clean:
	rm -f *.out
