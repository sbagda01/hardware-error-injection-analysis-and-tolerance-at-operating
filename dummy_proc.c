#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

volatile sig_atomic_t stop;

void inthand(int signum) 
{
    stop = 1;
}


int main(void) 
{
    printf("I am a process that declares an integer var and then loops forever until Ctrl-C is received.\n");
    printf("At every iteration the value is probed for change. I never change it.\n");

    signal(SIGINT, inthand);
    
    uint32_t i = 1;
    uint32_t x = i;
    int counter = 0;    
    printf("Var va: %p, Var init value: %u, Var size(bytes): %ld\n", (void *)&i, i, sizeof(i));
    printf("My pid: %ju\n", (uintmax_t)getpid());
    
    while (!stop) 
    {
	if(i != x)
	{
	    printf("\nValue changed externally! New value=%jx.\n", (uintmax_t)i);
	    x = i;
	}
	//printf("Counter value = %d\n", counter);
	//counter++;
	sleep(1);
    }
    printf("Finished.\n");
    return 0;
}
