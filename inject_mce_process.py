#!/usr/bin/python3

import sys
import os
import argparse
from subprocess import check_output
import tempfile


def translation(pid, va):
    """
        Uses the adress translation tool to return the PA at
        which we want to inject an mce.
    """
    try:
        output = check_output(["./va_pid_to_pa.out", "-p", str(pid), "-v", str(va)])
    except:
        print("Translation with ./va_pid_to_pa.out failed. Check the executable.")
        sys.exit(1);

    pa_hex_str = output.decode().split("PA:",1)[1]

    return pa_hex_str


def fix_ownership(path):
    """
        Change the owner of the file to SUDO_UID.
        This is needed becasue as the script runs as root
        the file owner changes to root when recreated.
    """
    uid = os.environ.get('SUDO_UID')
    gid = os.environ.get('SUDO_GID')
    if(uid is not None):
        os.chown(path, int(uid), int(gid))


def sub_and_inject(filepath, sub, pa):
    """
        Substitute the "sub" string with the "pa" string
        in the filepath in-place.
   """
    # Open and read the error tmpl
    err_file = open(filepath, 'r')
    tmpstr = err_file.read()
    err_file.close()

    #Replace the symbol with the PA
    tmpstr = tmpstr.replace(sub, pa)

    #Write to tmp file and invoke mce-inject
    with tempfile.NamedTemporaryFile('w+') as tmp:
        tmp.write(tmpstr)
        tmp.flush()
        mce_inject(tmp.name)


def mce_inject(filepath):
    try:
        output = check_output(["mce-inject", filepath])
    except:
        print("Error injection  with mce-inject  failed. Check the executable.")
        sys.exit(1);


def inject(pid, va, error_file):
    """
        First finds the PA, then modifies the erro desc. file
        and then injects the error. Finally check if the PA
        was changed during injection.
    """
    pa1 = translation(pid, va)
    if (int(pa1, 16) < 0):
        return 1

    sub_and_inject(error_file, '0x&', pa1)

    pa2 = translation(pid, va)
    if(int(pa2, 16) < 0):
        return 1

    if( pa1 != pa2):
        print("The injection was unsuccesful.")
    else:
        print("The injection was successful w.h.p")

    return 0

if __name__ == '__main__':
    parser = argparse.ArgumentParser(epilog="""
        Given the target process pid and va as well as mce-inject error description file,
        the script substitutes any "0x&" appearance in the error desc. file with the physical
        address and then invokes mce-inject. Needs root previlege to invoke the translation
        as well as the injection.
        """
        )
    parser.add_argument("-p", "--pid", required=True, help="PID of the target process.", type=int)
    parser.add_argument("-v", "--va", required=True, help="The virtual address(hex) of the target process.")
    parser.add_argument("-f", "--filepath", required=True, help="""
        The path to the error description file. This file will be modified. The initial
        content will be copied to filepath.init.
        """
        )
    args = parser.parse_args()
    if(os.getuid() != 0):
        print("I need root privileges to run!")
        sys.exit(0);
    status = inject(args.pid, args.va, args.filepath)
    if(status == 1):
        print("The virtual address is not mapped to a physical address. Abort.")
