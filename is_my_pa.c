#include "reusable.h"
#include "omp.h"

int main(int argc, char** argv)
{
    //This program takes a pid, a virtual address range and a physical address.
    //The program returns 0 if the physical address belongs to the virtual address range of that process.
    //The program returns 1 otherwise.

    if(argc != 5)
    {
	printf("4 arguments needed to run.\n");
	exit(0);
    }

    int pid = atoi(argv[1]);
    unsigned long long va_from = strtoull(argv[2], NULL, 10);
    unsigned long long va_to = strtoull(argv[3], NULL, 10);
    unsigned long long pa = strtoull(argv[4], NULL, 10);
    int pagesize = getpagesize();

    check_root_pid(pid);

    int found = 0;
    int local_found = 0;

    //Spread the addresses across the threads.
    #pragma omp parallel for private(local_found) reduction( || : found)
    for(unsigned long long va=va_from; va<=va_to; va=va+pagesize)
    {
	local_found = 0;
	unsigned long pa1 = va_to_pa((void*)va, pid);
	if(pa == pa1)
	{
	    local_found = 1;
	}
	found = found || local_found;
    }

    if(found)
        return 0;

    return 1;
}
