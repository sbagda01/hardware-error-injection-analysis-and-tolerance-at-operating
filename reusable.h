#ifndef VA_TO_PA_H
#define VA_TO_PA_H

#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <inttypes.h>

//Defined for Linux page entry 8 bytes.
#define PAGEMAP_LENGTH 8

//The number of translations to check if pa changes
#define CHCK_ADDR_CHANGE 10

// Input options for the execution
struct Options 
{
    pid_t pid;
    unsigned long long va;    
    int validation;
    int dummy_proc_validation;
};


void check_root_pid(pid_t);
unsigned long long get_page_frame_number_of_address(void* , pid_t);
unsigned long long va_to_pa(void*, pid_t);

#endif
