#!/usr/bin/python3

#Python imports
import sys
import os
import resource
import argparse
import fileinput
import random
import re

#Custom imports
from inject_mce_process import inject as inject_mce
from inject_mce_process import translation as va_to_pa
from maps_parse import parse_mem_maps


INFO="""
    This script will guide you through the steps of simple  MCE injection
    to a running process's memory. The script requires root privileges in order
    to run. For complete and manual error injection you can use inject_mce_process
    script.
"""


#The default directory for err. description files.
TMPL_DIR = sys.path[0] + "/err_templates/"


def file_ends_with_err(filename):
       return re.match(r'\w*.err$', filename)


class Mce:
    """MCE related options and functions mainly taken from mce-inject itself."""

    ERROR_TYPES = []

    err_type_choice = 0
    va = 0
    repeat = 1

    def __init__(self):
        self.ERROR_TYPES = []
        for filename in os.listdir(TMPL_DIR):
            if(file_ends_with_err(filename)):
                self.ERROR_TYPES.append(filename[:-len('.err')])


    def get_error_types_options(self):
        return _options_decode(options=self.ERROR_TYPES)


    def list(self, number):
        print("\tMCE %d descritpion:" % number)
        print("\tVA=%s." % hex(self.va))
        print("\tType=%s." % self.ERROR_TYPES[self.err_type_choice])
        print("\tRepeat=%d." % self.repeat)


    def select_error_tmpl(self):
        return self.ERROR_TYPES[self.err_type_choice] + '.err'


class MceSelection:
    """Mce selection attributes and methods.."""

    INJECTION_TYPES = ['random', 'list maps', 'already know va', 'random in section', 'exit']

    pid = 0
    inj_type_choice = 0
    mem_reg_choice = 0
    mces = []

    def get_mem_regions_list(self):
        dec = ''
        dec = '\033[1m'+"R# Address region            " + "Perm" + " F_offset " + "Dev   " + "Inode" + "\t\t\t    Pathname\n"
        dec = dec + '\033[0m'
        maps_file_name = "/proc/" + str(self.pid) + "/maps"
        with open(maps_file_name, 'r') as maps_file:
            mem_reg = 0
            for line in maps_file.readlines():
                dec = dec + str(mem_reg) + ": " + line
                mem_reg = mem_reg + 1
        return dec, range(0, mem_reg - 1)


    def get_injection_types_options(self):
        return _options_decode(options=self.INJECTION_TYPES)


    def list(self):
        print("Injection to process %d." % self.pid)
        print("Injection type=%s" % self.INJECTION_TYPES[self.inj_type_choice])

        if(self.inj_type_choice == 3):
            print("Mem section=%d" % self.mem_reg_choice)

        counter = 0
        for mce in self.mces:
            mce.list(counter)
            print()
            counter = counter + 1


    def execute(self):
        for mce in self.mces:
            err_dsc = TMPL_DIR + mce.select_error_tmpl()
            print("Injecting to %s" % hex(mce.va))
            for r in range(0, mce.repeat):
                print("Injection number %d" % r)
                inject_mce(pid=self.pid, va=hex(mce.va), error_file=err_dsc)


def _options_decode(options):
        dec = ''
        for number, option in enumerate(options):
            dec = dec +'\t' + str(number) + ":" + str(option) + '\n'
        return dec, range(0, len(options))


def check_input_hex(prompt, min_=None, max_=None, range_=None):
    while True:
        ui = input(prompt)
        try:
            ui = int(ui, 16)
        except ValueError:
            print("Input type must be %s." % "Hex")
            continue
        if (max_ is not None and ui > max_):
            print("Input must be <= %d." % max_)
        elif (min_ is not None and ui < min_):
            print("Input must be >= %d." % min_)
        elif (range_ is not None and ui not in range_):
            if (isinstance(range_, range)):
                print("Input must be in between %d and %d" % (range_.start, range_.stop-1))
            else:
                print("Input must be one of %s" % (list(range_)))
        else:
            return ui


def check_input(prompt, type_=None, min_=None, max_=None, range_=None):
    """
        A generic function to ask iser for input
        and the validate it.
    """
    if (min_ is not None and max_ is not None and max_ < min_):
        raise ValueError("min_ must be less than or equal to max_.")
    while True:
        ui = input(prompt)
        if (type_ is not None):
            try:
                ui = type_(ui)
            except ValueError:
                print("Input type must be %s." % type_)
                continue
        if (max_ is not None and ui > max_):
            print("Input must be <= %d." % max_)
        elif (min_ is not None and ui < min_):
            print("Input must be >= %d." % min_)
        elif (range_ is not None and ui not in range_):
            if (isinstance(range_, range)):
                print("Input must be in between %d and %d" % (range_.start, range_.stop-1))
            else:
                print("Input must be one of %s" % (list(range_)))
        else:
            return ui


def fill_mces(va_list):
    mces = []

    for va in va_list:
        print("MCE description for va=%s." % hex(va))

        mce = Mce()

        #Set the va
        mce.va = va

        # Select the error type for each va.
        err_type_options, err_range  = mce.get_error_types_options()
        mce.err_type_choice = check_input("Enter the error type.\nOptions:\n%sChoice:" % err_type_options,
                            type_=int, range_= err_range)

        #Repeatance
        number = check_input("How many times to repeat mce:", type_=int, min_=1)
        mce.repeat = number

        mces.append(mce)

    return mces


def check_valid_vas(pid, vas):
    """Tries to translate the vas to see if they are valid."""
    for va in vas:
        status = int(va_to_pa(pid, hex(va)), 16)
        if(status < 0):
            print("%s is invalid." % hex(va))
            return 1
    return 0


def create_random_vas(pid, range_, number=1):
    """Return a list with a number of va's that are valid."""
    SKIP_LIMIT = 50
    vas = []
    counter = 0
    skip = 0

    pagesize = resource.getpagesize()

    while True:
        #Generate random page va in range
        va = random.choice(range(range_.start, range_.stop, pagesize))

        #Verify that this VA is actually mapped to PA
        status = int(va_to_pa(pid, hex(va)), 16)
        if(status < 0):
            #Stop the process if takes too much iterations.
            skip = skip + 1
            if(skip >= SKIP_LIMIT):
                return vas
            continue

        #Get a random offset.
        va = va + random.choice(range(1, pagesize-1))

        #Keep a number of good va's
        vas.append(va)
        counter = counter + 1
        if(counter == number):
            return vas


def get_mces_from_user():
    #Inform the User
    print("Initially you will set up the injection environment. When you finish")
    print("you will be asked to confirm you selection in order to perform the injection.")

    #Start reading input
    mce_sel = MceSelection()
    mce_sel.pid = check_input("Enter the pid of the target process:", type_=int, min_=1)

    while True:
        #The list of vas to be created
        vas = []

        #What kind of injection do you want? 0.random vas 1.list maps 2.already has va, 3. select region from maps, 4. exit.
        inj_type_options, inj_range  = mce_sel.get_injection_types_options()
        mce_sel.inj_type_choice = check_input("Enter the injection type.\nOptions:\n%sChoice:" % inj_type_options,
                    type_=int, range_= inj_range)

        #In  case of random
        if(mce_sel.inj_type_choice == 0):
            #How many? create a list of random va's
            number = check_input("How many va's to use:", type_=int, min_=1)

            #Parse mem_regions
            mem_regions = parse_mem_maps(mce_sel.pid)

            #Try to change the region if can't find addresses
            vas_count = 0
            while(vas_count < number):
                #Select a region randomly
                region = random.randint(0, len(mem_regions)-2)
                ran = range(mem_regions[region].vstart, mem_regions[region].vend)
                vas.extend(create_random_vas(mce_sel.pid, number=number-vas_count, range_=ran))
                vas_count = len(vas)
            break

        #In case simply list maps
        if(mce_sel.inj_type_choice == 1):
            mem_reg_list, mem_reg_range  = mce_sel.get_mem_regions_list()
            print("The mappings of virtual memory regions:\n%s\n" % mem_reg_list)
            continue

        #In case of one already knows the va or va's.
        if(mce_sel.inj_type_choice == 2):
            #Three cases. One va, va range, va list.
            answ = check_input("Selection. 0:I have one va, 1:I have miltiple va's, 2:I have a range of va's.\nChoice:",
                                type_=int, range_=range(0,3))
            if(answ == 0):
                vas.append(check_input_hex("Enter the va of the target process as hex:"))
            if(answ == 1):
                list_hex = check_input("Enter a list of hex like:hex1,hex2,hex3:",type_=str)
                vas = [int(l, 16) for l in list_hex.split(',')]
            if(answ == 2):
                ran = check_input("Enter a range of hex like:low_hex,upper_hex:", type_=str)
                l = ran.split(',')
                vas = list( range( int(l[0], 16), int(l[1], 16) + 1))

            if(check_valid_vas(mce_sel.pid, vas) != 0):
                continue

            break

        #In case of section selection
        if(mce_sel.inj_type_choice == 3):

            #Parse mem_regions
            mem_regions = parse_mem_maps(mce_sel.pid)

            vas = None
            while vas is None:
                #Ask for section number, a random mce will be added to this section.
                mem_reg_list, mem_reg_range  = mce_sel.get_mem_regions_list()
                mce_sel.mem_reg_choice = check_input("Enter the memory region number.\nOptions:\n%sChoice:" % mem_reg_list,
                    type_=int, range_= mem_reg_range)

                #How many? create a list of random va's in specific section
                number = check_input("How many va's to use:", type_=int, min_=1)

                vas = create_random_vas(mce_sel.pid, number=number,
                            range_=mem_regions[mce_sel.mem_reg_choice].range_())
                if(len(vas) < number):
                    print("Can't find %d valid va's in the section %d" % (number, mce_sel.mem_reg_choice) )
            break

        #exit
        if(mce_sel.inj_type_choice == 4):
            sys.exit(0);


    #Ask for details of each mce
    mce_sel.mces = fill_mces(va_list=vas)

    return mce_sel


def user_interaction():
    """
        Hanldes the interaction with the user at high level.
    """
    print(INFO)

    mces = get_mces_from_user()

    print("####SUMMARY####")
    mces.list()
    confirm = check_input("Start injection:", type_=str, range_=['YES','n'])

    if(confirm != 'YES'):
        print("Abort...")
        sys.exit(1)

    mces.execute()


def main():
   user_interaction()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(epilog=INFO)
    #parser.add_argument("-p", "--pid", required=True, help="PID of the target process.", type=int)
    parser.add_argument("-t", "--err_tmpl", help="The directory with error templates")
    args = parser.parse_args()

    if(args.err_tmpl):
        TMPL_DIR = args.err_tmpl

    if(os.getuid() != 0):
        print("I need root privileges to run!")
        sys.exit(0);

    main()
