#include "reusable.h"

#define ORIG_BUFFER "Hello, World!"
#define NEW_BUFFER "Hello, Linux!"

void* create_buffer(char*);
int open_memory(void);
void seek_memory(int fd, unsigned long offset);
int pa_change_check(int snooze);
int self_mem_change_check(void);
int mem_write_check(unsigned long pa, void* va, void* data, int bytes);
void fetch_args(int, char**, struct Options* );

int main(int argc, char** argv)
{
    printf("VA+pid to PA tool. Use -h to list available options.\n");
    
    //Init the options
    struct Options opt;
    opt.pid = 0;
    opt.va = 0;
    opt.validation = 0;
    opt.dummy_proc_validation = 0;

    //Read the arguments into opt
    if(argc <= 1)
    {	    
	printf("Nothing will happen.\n");
	exit(0);
    }

    fetch_args(argc, argv, &opt);
    
   int status = 0;
    if (opt.validation) 
    {
        printf("Entering self validation process.\n");
	status |= pa_change_check(CHCK_ADDR_CHANGE);
	status |= self_mem_change_check();
	if(status != 0)
	{
	    fprintf(stderr, "Self validation is unsuccessful. There might be something wrong.\n");
	    exit(1);
	}
	printf("Self validation is successful.\n");	
    }
    
   if(opt.dummy_proc_validation)    
    {
	check_root_pid(opt.pid);
	printf("Modifying the memory of the dummy process.\n");	
	srand(time(NULL));
	int a = rand();
	printf("Writing integer with value %x.\n", a);
	mem_write_check(va_to_pa((void*)opt.va,opt.pid), &a, &a, sizeof(int));
    	printf("Modification complete.\n");
    }
    if(!opt.validation && !opt.dummy_proc_validation)
    {
	check_root_pid(opt.pid);
	unsigned long long pa = va_to_pa((void*)opt.va, opt.pid); 
	if(pa == 0)
	    printf("PA:-1\n");
	else
	    printf("PA:0x%llx\n", pa);
    }
    return 0;
}

/*
 *  Reads command line arguments
 *  and sets the Options
 * */
void fetch_args(int argc, char** argv, struct Options* opt)
{
    unsigned long long  status=0;
    for(int i = 1; i < argc; i++)
    {
	if(argv[i][0] != '-')
	{
	   fprintf(stderr, "All the options start with -\n"); 
	   exit(1);
	}
	switch(argv[i][1])
	{
		case 'p':
			if(i+1 > argc-1)
			{			
			    fprintf(stderr, "No pid given.\n"); 
			    exit(1);
			}
			status = atoi(argv[i+1]);
			if(status == 0)
			{
			    fprintf(stderr, "Wrong pid given.\n"); 
			    exit(1);
			}
			opt->pid = status;
			i++;
			break;
		case 'v':
			if(i+1 > argc-1)
			{			
			    fprintf(stderr, "No virtual address given.\n"); 
			    exit(1);
			}
			status = strtoull(argv[i+1], NULL, 16);
			if(status == 0)
			{
			    fprintf(stderr, "Wrong va given.\n"); 
			    exit(1);
			}
			opt->va = status;
			i++;
			break;
		case 's':
			opt->validation = 1; 
			break;
		case 'd':
			opt->dummy_proc_validation = 1;
			break;
		case 'h':
			printf("Available options:\n");
			printf("\t-p PID: the PID of the process to infect.\n");
			printf("\t-v VA: the virtual address(hex) of the process to infect.\n");
			printf("\t-s: Try to snoop PA changes. And validate a write to yourself.\n");
			printf("\t-d: Writes a random integer to the va of pid. Hint: use the dummy process.\n" );		
			break;
		default:
			fprintf(stderr, "Something is wrong with the args.\n");

	}	
    } 
}

/*
 *  Tries to verify the pa translation using 
 *  dynamic memory by writing/reading directly to /dev/mem.
 * */
int self_mem_change_check(void)
{
    // Create a buffer with some data in it
    void* buffer = create_buffer(ORIG_BUFFER);
    void* data = create_buffer(NEW_BUFFER);

    // Needed to get the address.
    unsigned long long pa;

    pa = va_to_pa(buffer, -1);
    
    int status = mem_write_check(pa, buffer, data, strlen(data) + 1);

    free(buffer);
    free(data);

    if(status != 0)
	printf("The memory change didn't work. Maybe you haven't recompiled the kernel with \
			\n\tCONFIG_STRICT_DEVMEM=n\n");

    return status; 
}

/*
 *  Tries to verify the PA stability.
 *  The OS can shuffle the PA's randomly
 *  depending on the system load.
 *
 *  @param snooze: The number
 *  of iterations. 
 * */
int pa_change_check(int snooze)
{
    // Create a buffer with some data in it
    void* buffer = create_buffer(ORIG_BUFFER);

    // Needed to get the address.
    unsigned long long pa;
    unsigned long long prev_pa = 0;

    // PA change check
    int iter = 0;
    int changes = -1;

    if (snooze > 1)
        printf("Verifying PA stability. \n");
    
    while (iter < snooze) 
    {
        pa = va_to_pa(buffer, -1);

        // let's count how many times the PA changes.
        if (pa != prev_pa) {
            changes++;
            prev_pa = pa;
        }

	iter++;
	printf("\r%d%%",(int)((float)iter/snooze*100));
	fflush(stdout);
	
	// let's wait a bit
        if(snooze > 1)
	    sleep(5);
        
    }
    printf("\n");

#ifdef DEBUG
    printf("Current Physical address: 0x%llx \n", pa);
    if(snooze > 1)
    	printf("It changed %d times.\n", changes);
#endif

    free(buffer);

    if(changes > 0)
	printf("The address is unstable: There is a possibility of wrong error injection. \
			\n\tTry to switch the swap off or memlock the targeting address.\n");

    return changes; 
}

/*
 *  Write the msg  to the PA in the memory
 *  and check if changed using VA.
 *  If va == data only performs the memory write.
 *
 *  @param pa: The physical address in the
 *  memory.
 *  @param va: The virtual address.
 *  @param data: What to write.
 *  @param bytes: How many bytes to check.
 * */
int mem_write_check(unsigned long pa, void* va, void* data, int bytes)
{
    // Open /dev/mem, seek the calculated offset.
    int mem_fd = open_memory();
    seek_memory(mem_fd, pa);

#ifdef DEBUG
    printf("Current state: %s\n", va);
    printf("Trying to write  through /dev/mem...\n");
    printf("Writing: %s\n", data);
#endif

    // Change the contents of the buffer by writing into /dev/mem
    if (write(mem_fd, data, bytes) < 0) 
    {
        fprintf(stderr, "Write failed: %s\n", strerror(errno));
        return 1;
    }

#ifdef DEBUG
    printf("New state: %s\n", va);
#endif

    // Clean up
    close(mem_fd);

    // Check if the data was written if pid==myid
    return memcmp(va, data, bytes);
}


/*
 * Returnes the address of a buffer
 * containing str.
 * */
void* create_buffer(char* str)
{
    size_t buf_size = strlen(str) + 1;

    // Allocate some memory to manipulate
    void* buffer = malloc(buf_size);
    if (buffer == NULL) 
    {
        fprintf(stderr, "Failed to allocate memory for buffer\n");
        exit(1);
    }
   
    // Add some data to the memory
    strncpy(buffer, str, strlen(str));

    return buffer;
}



/*
 * Returns the file descriptor
 * of the memory file on Linux.
 * */
int open_memory(void)
{
    // Open the memory (must be root for this)
    int fd = open("/dev/mem", O_RDWR);

    if (fd == -1) 
    {
        fprintf(stderr, "Error opening /dev/mem: %s\n", strerror(errno));
        exit(1);
    }

    return fd;
}

/*
 * Skips a part of a file up to
 * the offset.
 *
 * @param fd: The file descriptor.
 * @param offset: The offset into the file
 * in bytes.
 * */
void seek_memory(int fd, unsigned long offset)
{
    unsigned pos = lseek(fd, offset, SEEK_SET);

    if (pos == -1) 
    {
        fprintf(stderr, "Failed to seek /dev/mem: %s\n", strerror(errno));
        exit(1);
    }
}
